class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  require 'net/http'
  require 'will_paginate/array'
  
  include SessionsHelper
end
