class TagsController < ApplicationController

	def create
		if params["tags"].empty?
			flash[:alert] ="Please enter valid Tags !! "
			redirect_to controller: 'users', action: 'index'
		else
			uri = URI('https://pacific-fjord-97509.herokuapp.com/tags')
			tag_params = {"tags[]": params[:tags].split(","), user_id: session[:user_id]  }
			res = Net::HTTP.post_form(uri, tag_params)
			if res.code == "200"
				flash[:notice] = res.body
				redirect_to controller: 'users', action: 'index'
			else
				flash[:alert] = "Already Exist Tag"
				redirect_to controller: 'users', action: 'index'
			end
			# render 'users/show'
		end
	end

end