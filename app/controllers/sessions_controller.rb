class SessionsController < ApplicationController
  
	def new
		if session[:user_id]
			redirect_to controller: 'users', action: 'index'
		end
	end

	def create

		if params[:email].empty? || params[:password].empty?
			flash[:alert] = "Please Provide Valid Values"
			redirect_to controller: 'sessions', action: 'new'
		else	
			if !session[:user_id]	
				uri = URI('https://pacific-fjord-97509.herokuapp.com/login')
				user_params = {email: params[:email], password: params[:password]  }
				res = Net::HTTP.post_form(uri, user_params)

				if res.body != "null"
					@user = res.body
					@user = JSON.parse(@user)
					log_in @user
				# else
				# 	flash[:alert] = "Email or Password Not Match"
				# 	render 'sessions/new'
					# redirect_to controller: 'sessions', action: 'new'
				end
			else
				uri = URI('https://pacific-fjord-97509.herokuapp.com/users')
				user_params = {user_id: session[:user_id]}
				uri.query = URI.encode_www_form(user_params)
				@user = Net::HTTP.get_response(uri)
				@user = JSON.parse(@user.body)
			end

			if @user
				uri = URI('https://pacific-fjord-97509.herokuapp.com/tags')
				tag_params = {tag: '', user_id: @user["id"] }
				uri.query = URI.encode_www_form(tag_params)
				tag_res = Net::HTTP.get_response(uri)
				@tag = tag_res.body			
				@tag = JSON.parse(@tag)
				
				redirect_to controller: 'users', action: 'index', id: @user["id"]
			else
				flash[:alert] = "Email or Password Not Match"
				render 'sessions/new'
			end
		end
		# render 'static_pages/home'
	end

	def destroy
    log_out if logged_in?
    redirect_to root_url
  end
	
  end
  