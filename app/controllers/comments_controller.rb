class CommentsController < ApplicationController

	def create
		if params["comment"].empty?
			flash[:alert] = "Please Enter Valid Comment"

		else
			uri = URI('https://pacific-fjord-97509.herokuapp.com/comments')
			comment_params = {user_id: session[:user_id], commentable_type: params[:type], commentable_id: params[:id], comment: params[:comment] }
			res = Net::HTTP.post_form(uri, comment_params)
			@posted_comment = JSON.parse(res.body)

			# render 'questions/show'
		end
		

		if params[:type] == "Question"
			redirect_to controller: 'questions', action: 'index', id: params["id"]
		else
			redirect_to controller: 'answers', action: 'index', id: params["id"]
		end

	end

	def index
		if session[:user_id]
			uri = URI('https://pacific-fjord-97509.herokuapp.com/comments')
			comment_params = {commentable_id: params["id"], user_id: session[:user_id]  }
			uri.query = URI.encode_www_form(comment_params)
			@comment_res = Net::HTTP.get_response(uri)

			if @comment_res.code == "200"
				@comment_res = JSON.parse(@comment_res.body)

				@comment_type = params[:type]
				@comment_type_id = params[:type_id]

				render 'comments/comment'
			else
				@error_message = "Record does not exist !! "
				render 'errors/error'
			end

		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end
	end

	def destroy
		uri = URI('https://pacific-fjord-97509.herokuapp.com/comments/1')
		http = Net::HTTP.new(uri.host, uri.port)
		attribute_url = '?'
		body = { user_id: session["user_id"] , comment_id: params[:id] }
		attribute_url << body.map{|k,v| "#{k}=#{v}"}.join('&')
    request = Net::HTTP::Delete.new(uri.request_uri + attribute_url)
		response = http.request(request)
		flash[:notice] = response.body

		if params[:type] == "Question"	
			redirect_to controller: 'questions', action: 'index', id: params[:type_id] 
		else
			redirect_to controller: 'answers', action: 'index', id: params[:type_id]
		end
	end


	def edit
		if session[:user_id]
			uri = URI('https://pacific-fjord-97509.herokuapp.com/comments')
			comment_params = {user_id: session[:user_id], commentable_type: params[:type], commentable_id: params[:id], comment: params[:comment] }
			uri.query = URI.encode_www_form(comment_params)
			@comment_res = Net::HTTP.get_response(uri)

			if @comment_res.code == "200"
				@comment_res = JSON.parse(@comment_res.body)
				@comment_type = params[:type]
				@comment_type_id = params[:id]
			else
				@error_message = "Record does not exist !! "
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end		
	end
	
	def update

		if session[:user_id]
			uri = URI('https://pacific-fjord-97509.herokuapp.com/comments/1')
			req = Net::HTTP::Put.new(uri)
			req.set_form_data('user_id' => session[:user_id], 'comment_id' => params[:id], "comment": params[:value] )
			res = Net::HTTP.start(uri.hostname, uri.port) do |http|
				http.request(req)
			end

			if res.code == "200"
				res = JSON.parse(res.body)
				
				if params[:type] == "Question"	
					redirect_to controller: 'questions', action: 'index', id: params[:type_id] 
				else
					redirect_to controller: 'answers', action: 'index', id: params[:type_id]
				end
				
			else
				@error_message = "Record does not exist !! "
				render 'errors/error'
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end
	end

end