module SessionsHelper

	def log_in(user)
		session[:user_id] = user["id"]
		session[:email] = user["email"]
		session[:reputation] = user["reputation"]
		session[:password] = user["password"]
		session[:is_admin] = user["is_admin"]
	end

	def current_user
		if session[:user_id]
			@current_user ||=  session
		end
	end

	def logged_in?
		!current_user.nil?
	end

	def log_out
		session.delete(:user_id)
		@current_user = nil
	end

	def is_admin?
		session[:is_admin]
	end

end
